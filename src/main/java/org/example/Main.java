package org.example;

import org.example.config.AppConfig;
import org.example.config.DefaultAppConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        ContactsList contactsList = context.getBean(ContactsList.class);
        Scanner scanner = new Scanner(System.in);
        while (true){
            System.out.println("Введите: "+System.lineSeparator()
                    + "\t add - чтобы добавить контакт" + System.lineSeparator()
                    + "\t list - чтобы вывести список контактов" + System.lineSeparator()
                    + "\t del - чтобы удалить контакт по email" + System.lineSeparator()
                    + "\t exit - чтобы завершить работу");
            String command = scanner.nextLine();
            if (command.equals("list")) {
                contactsList.list();
            } else if (command.equals("add")) {
                contactsList.add();
            }else if (command.equals("del")){
                contactsList.del();
            } else if (command.equals("exit")) {
                break;
            }

        }
        ((AbstractApplicationContext)context).getBeanFactory().destroyBean(contactsList);

    }


}
