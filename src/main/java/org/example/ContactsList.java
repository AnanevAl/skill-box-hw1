package org.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

@Component
public class ContactsList {
    private List<Contact> contactList = new ArrayList<>();
    private ContactsReader contactsReader;
    private ContactsWriter contactsWriter;


    @Autowired
    public ContactsList(ContactsReader contactsReader, ContactsWriter contactsWriter) {
        this.contactsReader = contactsReader;
        this.contactsWriter = contactsWriter;
    }

    @PostConstruct
    private void init() {
        try {
            for (;;){
                Contact contact = contactsReader.read();
                if (contact == null) break;
                contactList.add(contact);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @PreDestroy
    private void save(){
        contactsWriter.write(contactList);
    }

    public void list() {
        for (Contact contact: contactList){
            System.out.println(contact.toString());
        }
    }

    public void add() {
        System.out.println("Введите контакт в формате ФИО;Номер телефона;адрес электронной почты");
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        String[] words = line.split(";");
        if (words.length != 3){
            System.out.println("Не верный формат");
            return;
        }
        Contact contact = new Contact();
        contact.setName(words[0]);
        contact.setPhone(words[1]);
        contact.setEmail(words[2]);
        contactList.add(contact);
    }

    public void del() {
        System.out.println("Введите email:");
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        contactList = contactList.stream()
                .filter(c -> !c.getEmail().equals(line))
                .collect(Collectors.toList());
    }
}
