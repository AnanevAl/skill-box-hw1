package org.example.config;

import org.springframework.context.annotation.*;

@ComponentScan("org.example")
@Configuration
@PropertySource("classpath:application-default.properties")
@Profile("default")
public class DefaultAppConfig {



}
